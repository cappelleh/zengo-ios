//
//  Bike.swift
//  zeronextgen
//
//  Created by Hans Cappelle on 15/12/2019.
//  Copyright © 2019 Hans Cappelle. All rights reserved.
//
import MapKit

class Bike: NSObject, MKAnnotation {
  let title: String?
  let locationName: String
  let coordinate: CLLocationCoordinate2D
  
  init(title: String, locationName: String, coordinate: CLLocationCoordinate2D) {
    
    self.title = title
    self.locationName = locationName
    self.coordinate = coordinate
    
    super.init()
  }
  
  var subtitle: String? {
    return locationName
  }
}
