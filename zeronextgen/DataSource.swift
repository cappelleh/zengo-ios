//
//  DataSource.swift
//  zeronextgen
//
//  Created by Hans Cappelle on 20/11/2019.
//  Copyright © 2019 Hans Cappelle. All rights reserved.
//

class DataSource {

    static let sharedInstance = DataSource()

    var data: [String:String] = [:]

    struct Keys {
        static let USERNAME = "username"
        static let PASSWORD = "secret"
        static let UNITNUMBER = "unitnumber"
        
        // user preferred units
        static let UNITS = "units"
        static let UNITS_KM = 0
        static let UNITS_MI = 1
        static let KM_TO_MILES : Double = 0.621371192
        
        // stored last location used in all tools
        static let ADDRESS = "address"
        static let LATITUDE = "latitude"
        static let LONGITUDE = "longitude"
        
        // current soc and range left
        static let RANGE = "range"
        static let SOC = "soc"
        
        // values stored for range calculator road and ride type
        static let ROAD_TYPE = "range-road"
        static let RIDE_TYPE = "range-ride"
        static let LANDSCAPE_TYPE = "range-landscape"
        static let WEATHER_TYPE = "range-weather"
    }
}
