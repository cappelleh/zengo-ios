//
//  ReferenceViewController.swift
//  zeronextgen
//
//  Created by Hans Cappelle on 20/11/2019.
//  Copyright © 2019 Hans Cappelle. All rights reserved.
//

import UIKit
import SwiftyMarkdown
import WebKit

class ReferenceViewController: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var webView : WKWebView! // changed reference view to online resource
    
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // for dark mode
        if #available(iOS 13, *) {
            self.view.backgroundColor = .systemBackground
        }
        
        // TODO integrate navigation bar with at least a loading indication
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)

        // loading of reference
        let url = URL(string: "https://fotoleer.wordpress.com/2021/01/17/exploded-views-installation-manuals-other-resources-for-zero-sr-f-en-sr-s-electric-motorcycle-owners/")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        //webView.allowsBackForwardNavigationGestures = true
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        // update loading indication here
        dismiss(animated: false, completion: nil)
    }

}
