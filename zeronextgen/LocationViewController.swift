//
//  LocationViewController.swift
//  zeronextgen
//
//  Created by Hans Cappelle on 20/11/2019.
//  Copyright © 2019 Hans Cappelle. All rights reserved.
//

import UIKit
import MapKit

import SwiftyJSON
import AFNetworking

class LocationViewController : UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    private var apiService = ApiService()
    private var weatherService = WeatherService()
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var locationView: UILabel!
    @IBOutlet weak var datePickerView: UIDatePicker!
    @IBOutlet weak var distanceView: UILabel!
    
    private var latitude: Double!
    private var longitude: Double!
    private var address: String!
    
    private var initialLocation: CLLocation!
    private var circle: MKCircle!
    private var bike: Bike!
    
    private let defaults = UserDefaults.standard // user preferences
    private var units: Int = DataSource.Keys.UNITS_KM
    
    // calculated total distance for plotted routes
    private var calculatedDistance: Double = 0
    private var displayDistance = "Total Distance 0 km"
    
    @objc func onPause() {
        // not needed?
    }
    
    @objc func onResume() {
        // this is triggered whenever app is resumed, so another is shown in between
        updateLocation()
        updateRangeRadius()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // this is triggered when navigation is used to show view again without leaving app
        updateLocation()
        updateRangeRadius()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // for dark mode
        if #available(iOS 13, *) {
            self.view.backgroundColor = .systemBackground
        }
        
        // these will trigger the onPause and onResume methods
        NotificationCenter.default.addObserver(self, selector: #selector(onPause), name:
            UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onResume), name:
            UIApplication.willEnterForegroundNotification, object: nil)
        
        // show location on map
        updateLocation()
        // mark radius on map based on range estimation config
        updateRangeRadius()
    }
    
    @IBAction func clear(){
        // resets plotted routes and total distance calculation
        calculatedDistance = 0
        displayDistance = units == DataSource.Keys.UNITS_KM ? "Total Distance \(calculatedDistance.format(f: ".0")) km"
            : "Total Distance \((calculatedDistance * DataSource.Keys.KM_TO_MILES).format(f: ".0")) mi"
        distanceView.text = displayDistance
        // removes all overlays
        let overlays = mapView.overlays
        mapView.removeOverlays(overlays)
    }
    
    @IBAction func datePicked(sender: UIDatePicker){
        // new date picked so retrieve API information for that date and plot on map
        apiService.history(
            username: defaults.string(forKey: DataSource.Keys.USERNAME) ?? "default",
            password: defaults.string(forKey: DataSource.Keys.PASSWORD) ?? "default",
            unitNumber: defaults.string(forKey: DataSource.Keys.UNITNUMBER) ?? "default",
            date: sender.date,
            success: { (response) -> () in self.showRouteForDate(result: response) },
            failure: { (error) -> () in print("Error: \(error!.localizedDescription)") }
        )
    }
    
    private func showRouteForDate(result: JSON) {
        // convert all points
        var coords:[CLLocationCoordinate2D] = []
        var previousLocation: CLLocation? = nil
        for res in result {
            let coord = CLLocationCoordinate2D(
                latitude: Double("\(res.1["latitude"])") ?? 0,
                longitude: Double("\(res.1["longitude"])") ?? 0)
            coords.append(coord)
            // calculate distance between plotted points
            let currentLocation = CLLocation(latitude: coord.latitude, longitude: coord.longitude)
            if( previousLocation != nil){
                calculatedDistance += currentLocation.distance(from: previousLocation!) / 1000 // returns distance in meters
            }
            previousLocation = currentLocation
        }
        let line = MKPolyline(coordinates: coords, count: coords.count)
        mapView.addOverlay(line)
        // update total distance displayed
        displayDistance = units == DataSource.Keys.UNITS_KM ? "Total Distance \(calculatedDistance.format(f: ".0")) km"
            : "Total Distance \((calculatedDistance * DataSource.Keys.KM_TO_MILES).format(f: ".0")) mi"
        distanceView.text = displayDistance
    }
    
    private func getValidRange() -> String {
        let storedRange = DataSource.sharedInstance.data[DataSource.Keys.RANGE]
        let currentSoc = DataSource.sharedInstance.data[DataSource.Keys.SOC]
        if( storedRange == nil || storedRange == "0" ){
            // calc here with some default values, just pic min range as worst case
            let currentChargePercentage = Float(currentSoc ?? "95")! / 100.0
            let calculatedRange = 143 * currentChargePercentage
            DataSource.sharedInstance.data[DataSource.Keys.RANGE] = calculatedRange.description
        }
        return storedRange ?? "0"
    }
    
    private func updateRangeRadius() {
        // get full range based on range estimator tool settings
        let estimatedRangeString = getValidRange()
        let range = Double(estimatedRangeString)! * 1000.0 as CLLocationDistance
        // adjust full range figure for current SOC
        let socString = DataSource.sharedInstance.data[DataSource.Keys.SOC]
        let soc = Double(socString ?? "100")! * 0.01
        let rangeLeft = range * soc
        addRadiusCircle(location: initialLocation, radius: rangeLeft)

        //let radius = range * soc
        //addRadiusCircle(location: initialLocation, radius: radius)
        //let viewRadius = range + defaultRegionRadius as CLLocationDistance
        let viewRadius = rangeLeft * 2 as CLLocationDistance
        // update map magnification based on that number also
        centerMapOnLocation(location: initialLocation, regionRadius: viewRadius)
        
        units = defaults.integer(forKey: DataSource.Keys.UNITS)
        let estimatedRange = Double(estimatedRangeString ?? "0")! // double value, not for map coordinates
        let rangeValue = ( units == DataSource.Keys.UNITS_KM ) ? "Estimated range of \(estimatedRange.format(f: ".0")) km" : "Estimated range of \((estimatedRange * DataSource.Keys.KM_TO_MILES).format(f: ".0")) mi"
        
        // TODO don't show this when no location found in shared instance
        // show bike on map
        if( bike != nil ){
            mapView.removeAnnotation(bike)
        }
        bike = Bike(title: "Your Bike",
                        locationName: rangeValue,
                        coordinate: CLLocationCoordinate2D(latitude: latitude as! CLLocationDegrees, longitude: longitude as! CLLocationDegrees))
        mapView.addAnnotation(bike)

    }
    
    private func addRadiusCircle(location: CLLocation, radius: CLLocationDistance){
        mapView.delegate = self
        // remove previous one first
        if( circle != nil ) {
            self.mapView.removeOverlay(circle)
        }
        circle = MKCircle(center: location.coordinate, radius: radius)
        self.mapView.addOverlay(circle)
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = UIColor.red
            circle.fillColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.1)
            circle.lineWidth = 1
            return circle
        } else if let polyline = overlay as? MKPolyline {
            let testlineRenderer = MKPolylineRenderer(polyline: polyline)
            testlineRenderer.strokeColor = .random() //.blue
            testlineRenderer.lineWidth = 2.0
            return testlineRenderer
        } else {
            return MKPolylineRenderer()
        }
    }
    
    private func updateLocation(){
        let latitudeString = DataSource.sharedInstance.data[DataSource.Keys.LATITUDE]
        let longitudeString = DataSource.sharedInstance.data[DataSource.Keys.LONGITUDE]
        self.address = DataSource.sharedInstance.data[DataSource.Keys.ADDRESS]
        
        // parse these values
        // TODO use current location when no location found in shared instance
        self.latitude = Double(latitudeString ?? "0")
        self.longitude = Double(longitudeString ?? "0")
        
        // update views
        locationView.text = "@ \(address ?? "unknown")"
        
        // set initial location
        initialLocation = CLLocation(latitude: latitude as! CLLocationDegrees, longitude: longitude as! CLLocationDegrees)
        centerMapOnLocation(location: initialLocation, regionRadius: defaultRegionRadius)
    }
    
    // TODO let user configure radius of map?
    let defaultRegionRadius: CLLocationDistance = 5000
    
    func centerMapOnLocation(location: CLLocation, regionRadius: CLLocationDistance) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
      mapView.setRegion(coordinateRegion, animated: true)
    }
    
}

// TODO move extension to single file instead
extension Double {
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}

extension CLLocationDistance {
    func inMiles() -> CLLocationDistance {
        return self*0.00062137
    }

    func inKilometers() -> CLLocationDistance {
        return self/1000
    }
}

// some extensions to generate random colors
extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static func random() -> UIColor {
        return UIColor(
            red: .random(),
            green: .random(),
            blue: .random(),
            alpha: 1.0
        )
    }
}
