//
//  WeatherService.swift
//  zeronextgen
//
//  Created by Hans Cappelle on 15/12/2019.
//  Copyright © 2019 Hans Cappelle. All rights reserved.
//

import SwiftyJSON
import AFNetworking

class WeatherService {
    
    private let manager = AFHTTPSessionManager() // for network connection
    private let params = ["units": "auto"]

    init() {
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.responseSerializer = AFJSONResponseSerializer()
        // TODO check how to configure baseUrl
        //manager.baseURL = "https://mongol.brono.com/"
    }
    
    func forecast(latitude: String, longitude: String,
                  success: @escaping (JSON) -> (),
                  failure: @escaping (Error?) -> ()) {
        
        let url = "https://api.forecast.io/forecast/bfd06b6715994c79a0b90499f5bb02a7/\(latitude),\(longitude)"
        // TODO deprecated
        manager.get(url,
            parameters: params,
            success:{
                (task: URLSessionDataTask!, responseObject: Any?) in
                print("success, got a weather response")
                success(JSON(responseObject))
                
            },
            failure: {
                (task: URLSessionDataTask!, error: Error?) in
                print("Error: \(error!.localizedDescription)")
                failure(error)
            }
        )
    }
    
}
