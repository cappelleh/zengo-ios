//
//  ViewController.swift
//  zeronextgen
//
//  Created by Hans Cappelle on 15/11/2019.
//  Copyright © 2019 Hans Cappelle. All rights reserved.
//

import UIKit
import SwiftyJSON

class ViewController: UIViewController {
    
    private var apiService = ApiService()
    private var unitNumber: String? = nil // fetched unitnumber, only kept for session
    private let defaults = UserDefaults.standard // user preferences
    
    private var iconClick = true // to toggle reveal hidden input
    
    @IBOutlet weak var usernameField : UITextField!
    @IBOutlet weak var passwordField : UITextField!
    @IBOutlet weak var statusView: UILabel!
    
    @IBOutlet weak var infoView : UILabel! // shows stored unit number
    
    @IBOutlet weak var progressView : CircularProgressBar! // progress bar
    
    @IBOutlet weak var mileageView : UILabel! // view for mileage
    private var mileage: Double = 0
    
    // other state indicator views
    @IBOutlet weak var droppedView : UIButton!
    @IBOutlet weak var pluggedView : UIButton!
    @IBOutlet weak var chargingView : UIButton!
    @IBOutlet weak var chargeCompleteView : UIButton!
    @IBOutlet weak var timeLeftView : UILabel!
    
    @IBOutlet weak var voltagrView : UILabel!
    @IBOutlet weak var locationView : UILabel!
    @IBOutlet weak var serialNumberView : UILabel!
    
    @IBOutlet weak var metricUnits: UIButton!
    @IBOutlet weak var imperialUnits: UIButton!
    private var units: Int = DataSource.Keys.UNITS_KM
    
    @objc func onPause() {
        // needed?
    }
    
    @objc func onResume() {
        print("view resumed so refreshing data")
        getUnitNumber()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // these will trigger the onPause and onResume methods
        NotificationCenter.default.addObserver(self, selector: #selector(onPause), name:
            UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onResume), name:
            UIApplication.willEnterForegroundNotification, object: nil)
        
        // had to remove background color on main view for dark mode,
        // this fixes issues on devices without dark mode
        if #available(iOS 13, *) {
            self.view.backgroundColor = .systemBackground
        }
    
        // check if username was stored before and recover here
        usernameField.text = defaults.string(forKey: DataSource.Keys.USERNAME)
        passwordField.text = defaults.string(forKey: DataSource.Keys.PASSWORD)
        
        // also restore preferred units here
        units = defaults.integer(forKey: DataSource.Keys.UNITS)
        imperialUnits.isSelected = units == DataSource.Keys.UNITS_MI
        metricUnits.isSelected = units == DataSource.Keys.UNITS_KM
        
        // init progress bar
        initProgressView()
        
        // try to restore unitNumber also, saving it is optional
        self.unitNumber = defaults.string(forKey: DataSource.Keys.UNITNUMBER)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // having a small delay and actually showing progress on start of app
            // resolves a progress bar that resets after reaching actual value
            self.getUnitNumber() // get data on startup if unitNumber is set
        }

    }
    
    @IBAction func useMetricUnits() {
        metricUnits.isSelected = true
        imperialUnits.isSelected = false
        units = DataSource.Keys.UNITS_KM
        defaults.set(units, forKey: DataSource.Keys.UNITS)
        showMileage(value: mileage)
    }
    
    @IBAction func useImperialUnits() {
        metricUnits.isSelected = false
        imperialUnits.isSelected = true
        units = DataSource.Keys.UNITS_MI
        defaults.set(units, forKey: DataSource.Keys.UNITS)
        showMileage(value: mileage)
    }
    
    @IBAction func iconAction(sender: AnyObject) {
        print("test touch down repeat")
        if(iconClick == true) {
            passwordField.isSecureTextEntry = false
        } else {
            passwordField.isSecureTextEntry = true
        }
        iconClick = !iconClick
    }
    
    private func initProgressView() {
        progressView.labelSize = 60
        progressView.lineWidth = 35
        progressView.safePercent = 30
        progressView.setProgress(to: 0, withAnimation: false)
    }
    
    @IBAction func storeAllValues() {
        
        // stores also password ... TODO check how this can be done encrypted
        defaults.set(passwordField.text, forKey: DataSource.Keys.PASSWORD)
        if( unitNumber != nil ){
            defaults.set(unitNumber, forKey: DataSource.Keys.UNITNUMBER)
        }
        print("secrets stored")
    }
    
    @IBAction func clearUnitNumber() {
        unitNumber = nil
        defaults.set(unitNumber, forKey: DataSource.Keys.UNITNUMBER)
        displayUnitNumber()
        defaults.set(nil, forKey: DataSource.Keys.PASSWORD)
        passwordField.text = nil
        defaults.set(nil, forKey: DataSource.Keys.USERNAME)
        usernameField.text = nil
    }
    
    @IBAction func getUnitNumber() {
        
        // store username, password isn't stored unless user requests
        defaults.set(usernameField.text, forKey: DataSource.Keys.USERNAME)
        
        // TODO check for input instead of using defaults here
        // if all valid hide keyboard
        passwordField.resignFirstResponder()
        
        // skip getting unitNumber when already fetched in session
        if( unitNumber != nil ){
            displayUnitNumber()
            continueWithUnitNumber()
            return
        }
        apiService.units(username: usernameField.text ?? "default",
                         password: passwordField.text ?? "default",
                         success: { (response) -> () in
                            
                // TODO for now limited to first bike only
                self.unitNumber = response[0]["unitnumber"].description
                self.displayUnitNumber()
                self.continueWithUnitNumber()
                            
            }, failure: { (error) -> () in
                self.statusView.text = "Error: \(error!.localizedDescription)"
        })
    }
    
    private func displayUnitNumber() {
        self.infoView.text = "Your unit No. is \(self.unitNumber ?? "missing")"
    }

    func continueWithUnitNumber(){
        apiService.lastTransmit(username : usernameField.text ?? "default",
                                password: passwordField.text ?? "default",
                                unitNumber: unitNumber ?? "default",
                                success: { (response) -> () in self.showResult(result: response) },
                                failure: { (error) -> () in self.statusView.text = "Error: \(error!.localizedDescription)"})
    }
    
    private func showMileage(value: Double){
        mileage = value
        let displayValue = units == DataSource.Keys.UNITS_KM ? value : value * 0.621371192
        self.mileageView.text = "\(Int(displayValue ?? 0)) \(units == DataSource.Keys.UNITS_KM ? "km" : "mi")"
    }
    
    private func showResult(result: JSON) {
        // update progress bar view here
        let progress = Double("\(result[0]["soc"])")
        if( progress != nil ){
            self.progressView.setProgress(to: progress!/100, withAnimation: true)
        }
        
        // show more information
        self.statusView.text = "Actual date \(formatDate(value: result[0]["datetime_actual"].description))"
        
        // rounded mileage field, no need to display with 2 positions
        showMileage(value: Double(result[0]["mileage"].description) ?? 0)
        
        self.droppedView.alpha = result[0]["tipover"].description == "1" ? 1 : 0.3
        self.chargeCompleteView.alpha = result[0]["chargecomplete"].description == "1" ? 1 : 0.3
        self.chargingView.alpha = result[0]["charging"].description == "1" ? 1 : 0.3
        self.pluggedView.alpha = result[0]["pluggedin"].description == "1" ? 1 : 0.3
        self.timeLeftView.alpha = self.chargingView.alpha
        self.timeLeftView.text = "\(result[0]["chargingtimeleft"].description) min"
        
        self.voltagrView.text = "\(result[0]["main_voltage"].description) V"
        self.locationView.text = "@ \(result[0]["address"].description)"
        self.serialNumberView.text = "S/N \(result[0]["name"].description)"
        
        // share location with other view controllers
        DataSource.sharedInstance.data[DataSource.Keys.ADDRESS] = result[0]["address"].description
        DataSource.sharedInstance.data[DataSource.Keys.LATITUDE] = result[0]["latitude"].description
        DataSource.sharedInstance.data[DataSource.Keys.LONGITUDE] = result[0]["longitude"].description
        DataSource.sharedInstance.data[DataSource.Keys.SOC] = result[0]["soc"].description
    }
    
    private func formatDate(value: String) -> String {
        // parse value
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyyMMddHHmmss"
        let date: Date? = dateFormatterGet.date(from: value)
        // format date
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd/MM/yyyy HH:mm:ss"
        return dateFormatterPrint.string(from: date!)
    }

}
