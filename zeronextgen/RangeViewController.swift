//
//  RangeViewController.swift
//  zeronextgen
//
//  Created by Hans Cappelle on 20/11/2019.
//  Copyright © 2019 Hans Cappelle. All rights reserved.
//

import UIKit

class RangeViewController : UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var percentageView : UILabel!
    @IBOutlet weak var sliderView : UISlider!
    @IBOutlet weak var rangeView : UILabel!
    @IBOutlet weak var alternativeRangeView : UILabel!
    
    @IBOutlet weak var pickerView : UIPickerView!
    private let pickerData = ["City", "City/Road", "City/Highway", "Road", "Highway"]
    private let rangeData : [Float] = [287, 216, 193, 174, 143] // default range values for given scenario's
    
    private let defaultRangeInKm : Float = 287
    
    private var currentChargePercentage : Float = 0.95 // selection, just a default to start with
    private var currentRangeInKm : Float = 287
    
    @IBOutlet weak var ridingStyleSwitch : UISwitch!
    @IBOutlet weak var landscapeSwitch : UISwitch!
    @IBOutlet weak var coldWeather : UIButton!
    @IBOutlet weak var mildWeather : UIButton!
    @IBOutlet weak var warmWeather : UIButton!
    
    private let defaults = UserDefaults.standard // user preferences
    private var units: Int = DataSource.Keys.UNITS_KM
    private var ignoreUpdates = false
    
    @objc func onPause() {
        // needed?
    }
    
    @objc func onResume() {
        // this is triggered whenever app is resumed, so another is shown in between
        getCurrentSocValue()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // this is triggered when navigation is used to show view again without leaving app
        getCurrentSocValue()
    }
    
    override func viewDidLoad() {
     
        // for dark mode
        if #available(iOS 13, *) {
            self.view.backgroundColor = .systemBackground
        }
        
        // these will trigger the onPause and onResume methods
        NotificationCenter.default.addObserver(self, selector: #selector(onPause), name:
            UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onResume), name:
            UIApplication.willEnterForegroundNotification, object: nil)
        
        getCurrentSocValue()
        
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
    }
    
    private func getCurrentSocValue() {
        // get current units preference
        units = defaults.integer(forKey: DataSource.Keys.UNITS)
        
        // also get other last used values from prefs
        let roadType = defaults.integer(forKey: DataSource.Keys.ROAD_TYPE)
        let rideType = defaults.integer(forKey: DataSource.Keys.RIDE_TYPE)
        let landscapeType = defaults.integer(forKey: DataSource.Keys.LANDSCAPE_TYPE)
        let weatherType = defaults.integer(forKey: DataSource.Keys.WEATHER_TYPE)
        // update separate views according
        // TODO check if I can block triggering calculate on these updates
        ignoreUpdates = true
        pickerView.selectRow(roadType, inComponent: 0, animated: false)
        currentRangeInKm = rangeData[roadType]
        ridingStyleSwitch.setOn(rideType == 1, animated: false)
        landscapeSwitch.setOn(landscapeType == 1, animated: false)
        switch(weatherType) {
            case 2 :
                coldWeather.isSelected = true
                mildWeather.isSelected = false
                warmWeather.isSelected = false
            case 1 :
                coldWeather.isSelected = false
                mildWeather.isSelected = true
                warmWeather.isSelected = false
            default :
                coldWeather.isSelected = false
                mildWeather.isSelected = false
                warmWeather.isSelected = true
        }
        ignoreUpdates = false
        
        let socString = DataSource.sharedInstance.data[DataSource.Keys.SOC]
        currentChargePercentage = Float(socString ?? "95")! / 100.0
        sliderView.value = currentChargePercentage
        calculate()
    }
    
    @IBAction func sliderValueChanged(slider: UISlider) {
        currentChargePercentage = slider.value
        percentageView.text = "\(Int(100 * currentChargePercentage))%"
        // TODO also implement settings and allow for conversion to miles
        
        // get baseline
        var calculatedRangeInKm = Double(currentRangeInKm * currentChargePercentage)
        // correct for parameters
        calculatedRangeInKm = calculatedRangeInKm * (ridingStyleSwitch.isOn ? 0.9 : 1)
        calculatedRangeInKm = calculatedRangeInKm * (landscapeSwitch.isOn ? 0.9 : 1)
        calculatedRangeInKm = calculatedRangeInKm * (coldWeather.isSelected ? 0.9 : 1)
        calculatedRangeInKm = calculatedRangeInKm * (mildWeather.isSelected ? 0.95 : 1)
        
        // use range and alternative range figures based on user preference, calculated valus is always in KM
        if( units == DataSource.Keys.UNITS_KM){
            rangeView.text = "\(Int(calculatedRangeInKm)) km"
            alternativeRangeView.text = "\(Int(calculatedRangeInKm * DataSource.Keys.KM_TO_MILES)) miles"
        } else {
            alternativeRangeView.text = "\(Int(calculatedRangeInKm)) kilometers"
            rangeView.text = "\(Int(calculatedRangeInKm * DataSource.Keys.KM_TO_MILES)) mi"
        }
        
        // store for use in mapview
        DataSource.sharedInstance.data[DataSource.Keys.RANGE] = calculatedRangeInKm.description
    }
    
    @IBAction func stateChanged(switchState: UISwitch) {
        // prevent loops by manually restoring from user preferences
        if( ignoreUpdates ){
            return
        }
        // update in preferences
        defaults.set(ridingStyleSwitch.isOn, forKey: DataSource.Keys.RIDE_TYPE)
        defaults.set(landscapeSwitch.isOn, forKey: DataSource.Keys.LANDSCAPE_TYPE)
        
        calculate()
    }
    
    @IBAction func weatherSelectionChange(_ sender: UIButton, forEvent event: UIEvent){
        // prevent loops by manually restoring from user preferences
        if( ignoreUpdates ){
            return
        }
        switch sender {
        case coldWeather:
            coldWeather.isSelected = true
            mildWeather.isSelected = false
            warmWeather.isSelected = false
            defaults.set(2, forKey: DataSource.Keys.WEATHER_TYPE)
        case mildWeather:
            coldWeather.isSelected = false
            mildWeather.isSelected = true
            warmWeather.isSelected = false
            defaults.set(1, forKey: DataSource.Keys.WEATHER_TYPE)
        default:
            coldWeather.isSelected = false
            mildWeather.isSelected = false
            warmWeather.isSelected = true
            defaults.set(0, forKey: DataSource.Keys.WEATHER_TYPE)
        }
        calculate()
    }
    
    private func calculate(){
        sliderValueChanged(slider: sliderView) // trigger the actual update
    }
    
    // Capture the picker view selection
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // prevent loops by manually restoring from user preferences
        if( ignoreUpdates ){
            return
        }
        // This method is triggered whenever the user makes a change to the picker selection.
        // The parameter named row and component represents what was selected.
        currentRangeInKm = rangeData[row]
        defaults.set(row, forKey: DataSource.Keys.ROAD_TYPE)
        calculate()
    }
    
    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
}
