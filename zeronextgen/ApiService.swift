//
//  ApiService.swift
//  zeronextgen
//
//  Created by Hans Cappelle on 15/12/2019.
//  Copyright © 2019 Hans Cappelle. All rights reserved.
//

import SwiftyJSON
import AFNetworking

class ApiService {
    
    private let manager = AFHTTPSessionManager() // for network connection
    private let endpoint = "https://mongol.brono.com/mongol/api.php"
    private let dateFormatterStart = DateFormatter()
    private let dateFormatterEnd = DateFormatter()
    
    // demo example data
    private let demodataTransmit = "[{\"unitnumber\":\"123456\",\"name\":\"538ZFAZ74LCK00000\",\"unittype\":\"5\",\"unitmodel\":\"6\",\"mileage\":\"4875.26\",\"software_version\":\"190430\",\"logic_state\":\"2\",\"reason\":\"2\",\"response\":\"0\",\"driver\":\"0\",\"longitude\":\"2.349014\",\"latitude\":\"48.864716\",\"altitude\":\"52\",\"gps_valid\":\"0\",\"gps_connected\":\"0\",\"satellites\":\"0\",\"velocity\":\"0\",\"heading\":\"168\",\"emergency\":\"0\",\"shock\":\"\",\"ignition\":\"0\",\"door\":\"0\",\"hood\":\"0\",\"volume\":\"0\",\"water_temp\":\"\",\"oil_pressure\":\"0\",\"main_voltage\":13.18,\"analog1\":\"0.09\",\"siren\":\"0\",\"lock\":\"0\",\"int_lights\":\"0\",\"datetime_utc\":\"20191116090806\",\"datetime_actual\":\"20191116210319\",\"address\":\"Paris\",\"perimeter\":\"\",\"color\":2,\"soc\":64,\"tipover\":0,\"charging\":1,\"chargecomplete\":0,\"pluggedin\":1,\"chargingtimeleft\":120}]"
    private let demodataUnits = "[{\"unitnumber\": \"798124\",\"name\": \"538ZFAZ74LCK12292\",\"address\": \"+1\",\"vehiclemodel\": \"UPDATE\",\"vehiclecolor\": \"\",\"unittype\": \"5\",\"icon\": \"0\",\"active\": \"1\",\"unitmodel\": \"6\",\"custom\": []}]"
    private let demoHistory = "[{\"latitude\":\"48.864716\",\"longitude\":\"2.349014\"},{\"latitude\":\"48.064716\",\"longitude\":\"2.549014\"},{\"latitude\":\"47.864716\",\"longitude\":\"2.049014\"},{\"latitude\":\"48.064716\",\"longitude\":\"2.149014\"},{\"latitude\":\"48.464716\",\"longitude\":\"2.249014\"},{\"latitude\":\"48.864716\",\"longitude\":\"2.349014\"}]"
    
    init() {
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.responseSerializer = AFJSONResponseSerializer()
        // TODO check how to configure baseUrl
        //manager.baseURL = "https://mongol.brono.com/"
        dateFormatterStart.dateFormat = "yyyyMMdd000000"
        dateFormatterEnd.dateFormat = "yyyyMMdd235959"
    }
    
    func history(username:String, password: String, unitNumber: String, date: Date,
                 success: @escaping (JSON) -> (),
                 failure: @escaping (Error?) -> ()) {
        
        if( username == "demo" && password == "demo" ){
            success(JSON.init(parseJSON: demoHistory))
            return
        }
        
        let params = [
            "commandname": "get_history",
            "format":"json",
            "user":"\(username)",
            "pass":"\(password)",
            "unitnumber": "\(unitNumber)",
            "start": "\(dateFormatterStart.string(from:date))",
            "end": "\(dateFormatterEnd.string(from:date))"
        ]
        // FIXME this is deprecated
        manager.get(endpoint,
                    parameters: params,
                    success:{
                        (task: URLSessionDataTask!, responseObject: Any?) in
                        print("success, got a history response")
                        success(JSON(responseObject))
                        
        },
                    failure: {
                        (task: URLSessionDataTask!, error: Error?) in
                        print("Error: \(error!.localizedDescription)")
                        failure(error)
        })
    }
    
    func lastTransmit(username:String, password: String, unitNumber:String,
                      success: @escaping (JSON) -> (),
                      failure: @escaping (Error?) -> ()) {
        
        if( username == "demo" && password == "demo" ){
            success(JSON.init(parseJSON: demodataTransmit))
            return
        }
        
        let params = [
            "commandname": "get_last_transmit",
            "format":"json",
            "user":"\(username)",
            "pass":"\(password)",
            "unitnumber": "\(unitNumber)"
        ]
        // FIXME this is deprecated
        manager.get(endpoint,
                    parameters: params,
                    success:{
                        (task: URLSessionDataTask!, responseObject: Any?) in
                        print("success, got a last_transmit response")
                        success(JSON(responseObject))
                        
        },
                    failure: {
                        (task: URLSessionDataTask!, error: Error?) in
                        print("Error: \(error!.localizedDescription)")
                        failure(error)
        })
    }
    
    func units(username: String, password:String,
               success: @escaping (JSON) -> (),
               failure: @escaping (Error?) -> ()) {
        
        if( username == "demo" && password == "demo" ){
            success(JSON.init(parseJSON: demodataUnits))
            return
        }
        
        // get unit number for this user
        let params = [
            "commandname": "get_units",
            "format":"json",
            "user":"\(username)",
            "pass":"\(password)"
        ]
        // FIXME this is deprecated
        manager.get(endpoint,
        parameters: params,
        success:{
            (task: URLSessionDataTask!, responseObject: Any?) in
            print("success, got a units response")
            success(JSON(responseObject))
        },
        failure: {
            (task: URLSessionDataTask!, error: Error?) in
            print("Error: \(error!.localizedDescription)")
            failure(error)
        })
    }
    
    
}
