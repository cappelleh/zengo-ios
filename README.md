# ZenGo

## About this App

App to connect with public API for Zer0 next gen SR/F and SR/S electr1c m0t0rcycles.
This builds upon the information you see when using the "remote connect" functionality in the official app.
Showing more data and adding more configuration options and features. 

Non Zer0 SR-platform users can benefit from the reference manual in the app containing general information about the different platforms, firmware versions and recalls.
Also the range calculator is currently based on the 14.4kWh battery pack configuration which is also available for the Zer0 S, SR, DS and DSR models.

Android app is available from the Google Play Store, see https://play.google.com/store/apps/details?id=be.hcpl.android.zengo

![Android screenshot of overview](https://bitbucket.org/cappelleh/zengo/raw/3ec93942dd2c4aaf1885590801e1d2debc36f7a1/screenshots/Screenshot_1575383909_resized.png)
![Android screenshot of range estimator](https://bitbucket.org/cappelleh/zengo/raw/3ec93942dd2c4aaf1885590801e1d2debc36f7a1/screenshots/Screenshot_1575383914_resized.png)
![Android screenshot of bike location](https://bitbucket.org/cappelleh/zengo/raw/3ec93942dd2c4aaf1885590801e1d2debc36f7a1/screenshots/Screenshot_1575383917_resized.png)

iOS app is available from this link https://apps.apple.com/be/app/zerong/id1488172044?l=nl

![iPhone 11 screenshot of overview](https://bitbucket.org/cappelleh/zengo/raw/75c1821fa599044dfc6867b1338d139529372f59/screenshots/resized_Simulator%20Screen%20Shot%20-%20iPhone%2011%20Pro%20Max%20-%202019-11-23%20at%2009.06.11.png)
![iPhone 11 screenshot of range estimator](https://bitbucket.org/cappelleh/zengo/raw/75c1821fa599044dfc6867b1338d139529372f59/screenshots/resized_Simulator%20Screen%20Shot%20-%20iPhone%2011%20Pro%20Max%20-%202019-11-23%20at%2009.06.16.png)
![iPhone 11 screenshot of bike location](https://bitbucket.org/cappelleh/zengo/raw/75c1821fa599044dfc6867b1338d139529372f59/screenshots/resized_Simulator%20Screen%20Shot%20-%20iPhone%2011%20Pro%20Max%20-%202019-11-23%20at%2009.06.20.png)

## Resources

### API discovery as posted on electricmotorcyleforum.com

Remote service was discovered and published on this forum dedicated to electric motorcycles: https://electricmotorcycleforum.com/boards/index.php?topic=9520.0

> Turns out you can also use a plain URL in your browser : 
>
> https://mongol.brono.com/mongol/api.php?commandname=get_units&format=json&user=yourusername&pass=yourpass
>
> to get your unitnumber you have to use in the URL below
>
> https://mongol.brono.com/mongol/api.php?commandname=get_last_transmit&format=json&user=yourusername&pass=yourpass&unitnumber=0000000

### Documentation from starcom systems

See this published document (also in the downloads section of this app) with information on how 
starcom systems colaborated with Zer0 motorcycles to create the next gen connected system:
https://www.starcomsystems.com/wp-content/uploads/2019/05/The-Worlds-First-Connected-Motorcycle.pdf

So this system is based on the helios system that starcom sells. For documentation on that helios syste check: https://www.starcomsystems.com/helios/

Starcom also released this blog post on working with Zer0 and the results (again can be downloaded as a PDF from download section):
https://www.starcomsystems.com/2019/03/28/the-first-smart-connected-motorcycle-effortless-connection/

### Official Zer0 Resources

See their website - LINK REMOVED

## The API, what we know so far

To get json information back you can query using https://mongol.brono.com/mongol/api.php?commandname=get_units&format=json&user=yourusername&pass=yourpass first to get the unitnumber that applies to your configuration.
Once you have that you can trigger other commands like `get_userinfo`, `get_last_transmit`, `get_commands` and `get_history`.  

## Other Apps

Official Zer0 app for pre next gen motorcycles is and for Next gen motorcycles is available in Google Play Store and Apple App Store. LINKS REMOVED

Starcom have this app https://play.google.com/store/apps/details?id=com.starcomsystems.olympiatracking
and this one https://play.google.com/store/apps/details?id=com.starcomsystems.olympiarc

ZeroSpy is an Android app that supports the original zer0 system https://play.google.com/store/apps/details?id=com.bsc101.zerospy

A collection of other open source projects from Zer0 Enthousiasts https://github.com/zero-motorcycle-community

### Used Libraries for Apps

Android specific libraries used

- retrofit for network calls https://square.github.io/retrofit/
- icon generator https://romannurik.github.io/AndroidAssetStudio (also part of android studio)
- markdown implementation for Android https://github.com/noties/Markwon

Also I'm and Android developer so had to look up quite a bit for the iOS project. In the end this tutorial gave me the best information 
https://www.raywenderlich.com/5993-your-first-ios-and-uikit-app/lessons/1

iOS specific libraries and resources used

- network with AFNetworking lib from https://github.com/AFNetworking/AFNetworking
- for converting response to json https://github.com/SwiftyJSON/SwiftyJSON
- circular progress bar in swift https://codeburst.io/circular-progress-bar-in-ios-d06629700334
- icon generator https://appicon.co/
- color converter https://www.uicolor.xyz/#/hex-to-ui
- markdown lib https://github.com/SimonFairbairn/SwiftyMarkdown
- used system images for iOS so far https://stackoverflow.com/questions/23409610/how-to-use-default-ios-images

## Contributions

### How to become a test user

Both Android and iOS versions of the app have a beta test version released just before the actual full release goes live. This is done to exclude bigger issues to be released in the production version. 

**For iOS** you can become a test user by installing the [test flight app from the app store](https://apps.apple.com/us/app/testflight/id899247664) and then sending me an e-mail at hans-AT-hcpl.be to ask for an invite. Once the invite is accepted you'll receive an update on the given e-mail address and you'll find the test app in the testflight app where you can install or remove it. This overrides the production app you might already have installed. Information shouldn't be lost and you should be able to switch between test and production whenever you want, as longs as there is a test version available. 

**For Android** you can [register yourself using this link](https://play.google.com/apps/testing/be.hcpl.android.zengo). Once that is done you'll find an update for the app in the Google Play Store on your device. It's possible that you have to restart the play store app to receive this update.

### How to Contribute

Contributions are more than welcome. If you can't program there are plenty other options like providing translations, filing bugs, creating feature requests, ...

For those that want to contribute code for iOS I prefer changes created with Xcode 11. The projet should open and (mostly) run from Xcode 10 but I noticed some storyboard issues jumping around. Like the system icons are gone (since added in Xcode 11). But if Xcode 10 is your only option you should still be able to contribute. Just try to stay away from the storyboard.

### What TODO

See issue tracker for this project https://bitbucket.org/cappelleh/zengo/issues?status=new&status=open

## iOS App Version History

### 0.1.0

Initial app release for iOS to allow users to get basic information using public API. 

- retrieve unit number and details after logon
- visualize SoC with progress bar
- display mileage returned from services
- show warning when bike is dropped
- added icons for showing plugged in and charge state
- allow for local storage for quick updates

### 0.1.1

Changes done to get passed the review process of Apples App Store

- provide demo login
- adapt layout to device screen resolution

### 0.2.0

Several bug fixes and improvements

- retrieve data when resuming app/view
- small visual layout improvements
- several fixes done for dark mode support
- implemented reference manual using tab view and markdown

### 0.3.0

Bugfixes, range estimation tool and bikes location on map

- fixes for markdown reference content
- range estimation tool added
- added VIN breakdown information
- show bike location on map
- added missing plugged in icon
- more layout improvements
- weather forecast for bikes location

### 0.4.0 

bugfixes

- dark mode fix for new screens
- icon update

### 1.0

First Major release. More bugfixes and some new features

- show current range figure on map (based on estimator tool settings)
- small layout changes for weather views
- initialise range estimator tool with current SOC value if available
- added user preferred units
- restore type of riding preferences on range estimator tool

### 1.1 

- app about information added to reference
- layout update for location using @ instead of full prefix
- changed order or road types to be sorted on range instead of name
- update state label on overview also on valid responses

### 2.0

Implemented history showing on map by date selection

- added date selector on map to show route of that day
- internal code improvements
- fixed some misplaced text in reference manual
- fixed issue with zoom level on map when no range calculated yet
- reduced app logging 

### 2.1

Bugfixes and reference improvements

- moved datepicker to bottom of map not to cover current location
- added warning about 14.4kWh battery pack based to range calculator
- added Zer0 platform breakdown to reference manual
- added firmware upgrade instruction to reference manual
- added recall information to reference manual

### 2.2

small improvements

- allow showing current password
- show last timestamp in current timezone and formatted

### 2.4

map routing feature request changes

- plot different day routes in different (random) colors
- calculated total distance for plotted routes

### 3.0

several bugfixes and UI improvements

- fix mileage conversion on overview landing view
- changed layout of location view and datepicker
- added clear option to location view to remove plotted routes
- update reference to link to online resource instead


## Android App Version History

### 0.1.0

Initial release with minimum functionality. Required user to enter username and password on each use of app for example. 

### 0.2.0 

Fingerprint added so input can be stored. Overview of changes:

- fingerprint implementation to store password securely
- loading animation on getting data
- required network traffic for refresh reduced
- visualise error messages from network calls

### 0.3.0

Update visualisation of data. Overview of changes:

- show location of bike on map
- circular progress for SoC
- improved data visualisation

### 0.4.0

More helpful information added to app. 

- most important specifications added to help section
- VIN breakdown added to reference section
- linked to Zer0 nextgen app and gates belt tension app
- about section added to app
- tabs prepared for range estimation and map views
- layouts aligned with iOS layout

### 1.0

More tools added and aligned with iOS version

- range estimation tool added
- several layout fixes
- show location on map including current range
- reduced required network calls on resume
- app about information added to reference
- show weather forecast on map
- fix crash on quickly changing tabs

### 1.1

- fixed keyboard open on resume blocking overview
- fixed no result shown on resume of range calculator
- text fixed on reference documentation
- fixed calculation of range falling back to default value

### 1.2

- demo login added for new users w/o Zer0
- dutch translations added
- small screen improvements for range and map

### 2.0

Added route history on map

- small code and layout improvements
- added datepicker to map view to render a route for selected date

### 2.1

bugfixes

- date fixed
- recalc on resume fixed

### 2.2 

small improvements and reference information update

- allow showing current password
- show last timestamp in current timezone and formatted on user request
- added warning about 14.4kWh battery pack based to range calculator
- added Zer0 platform breakdown to reference manual
- added firmware upgrade instruction to reference manual
- added recall information to reference manual

### 2.3

keep screen on feature and bug fixes

- bugfix added reveal password option to layout group
- added keep screen on feature on user request

### 2.4

bugfix

- fixed keep screen on feature

### 2.5

reference updates

- add password reset information to reference documentation
- add specific vin details on SR/F vs SR/S model

### 2.6

bugfixes and small improvements

- fix for small screen rendering of range estimator view
- removed fab since feedback option also available from menu
- routes now printed in random color changing for each day/fetch
- made random color route an option (on by default)
- fix show/hide clear option from location fragment
- show total distance for plotted routes on map

### 3.0

removed all Zer0 references from app

- no more link to Zer0 app within app menu
- renamed app to ZeNGo
- some code cleanup
